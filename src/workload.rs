//! Abstractions over Kubernetes Workloads.

use k8s_openapi::api::{
    apps::v1::{Deployment, StatefulSet},
    core::v1::{Container, EnvVar, PodSpec, SecretKeySelector},
};
use kube::ResourceExt;

use crate::{error::KubeFields, Error};

/// A Kubernetes Workload.
/// Workloads can be Deployments or StatefulSets. (Or others, but those are not implemented within this crate.)
pub trait Workload: Send + Sync {
    /// Retrieves all containers in a given workload. This includes both regular containers and init containers.
    fn get_all_containers(&self) -> Result<Vec<&Container>, Error>;

    /// Returns the name of the namespace that contains the workload.
    fn namespace(&self) -> Result<String, Error>;

    /// Within a Workload definition, try to find the [`SecretKeySelector`] that refers to an environment variable fulfilling a given condition.
    /// e.g.
    /// ```no_run
    /// use kube_discovery::*;
    /// use kube_discovery::kube::{Client, Config};
    /// use kube_discovery::k8s_openapi::api::apps::v1::Deployment;
    /// async {
    ///     let sample_config = Config::infer().await.unwrap();
    ///     let kube_client = Client::try_from(sample_config).unwrap();
    ///     let deployment = LabelSelector("env=example").find_resource::<Deployment>(&kube_client).await.unwrap();
    ///     let secret = deployment.find_secret_ref("PASSWORD");
    /// };
    /// ```
    fn find_secret_ref(&self, env_var_name: &str) -> Result<SecretKeySelector, Error> {
        let all_containers = self.get_all_containers()?;

        let mut all_env_vars = all_containers
            .into_iter()
            .filter_map(|container| container.env.as_ref())
            .flatten();

        let matching_env_var = all_env_vars
            .find(|env_var| env_var.name == env_var_name)
            .cloned()
            .ok_or(Error::DataNotFound)?;

        matching_env_var
            .value_from
            .ok_or(Error::FieldNotFound(KubeFields::EnvVarSource))?
            .secret_key_ref
            .ok_or(Error::FieldNotFound(KubeFields::SecretKeySelector))
    }

    /// Find the value of an environment variable within the Workload.
    fn find_env_var(&self, env_var_name: &str) -> Result<EnvVar, Error> {
        let all_containers = self.get_all_containers()?;
        all_containers
            .into_iter()
            .filter_map(|container| container.env.as_ref())
            .flatten()
            .find(|env_var| env_var.name == env_var_name)
            .cloned()
            .ok_or(Error::DataNotFound)
    }
}

impl Workload for Deployment {
    fn get_all_containers(&self) -> Result<Vec<&Container>, Error> {
        let pod_spec = self
            .spec
            .as_ref()
            .ok_or(Error::FieldNotFound(KubeFields::DeploymentSpec))?
            .template
            .spec
            .as_ref()
            .ok_or(Error::FieldNotFound(KubeFields::PodSpec))?;

        Ok(get_all_containers_in_pod_spec(pod_spec))
    }

    fn namespace(&self) -> Result<String, Error> {
        <Self as ResourceExt>::namespace(self).ok_or(Error::FieldNotFound(KubeFields::Namespace))
    }
}

impl Workload for StatefulSet {
    fn get_all_containers(&self) -> Result<Vec<&Container>, Error> {
        let pod_spec = self
            .spec
            .as_ref()
            .ok_or(Error::FieldNotFound(KubeFields::StatefulSetSpec))?
            .template
            .spec
            .as_ref()
            .ok_or(Error::FieldNotFound(KubeFields::PodSpec))?;

        Ok(get_all_containers_in_pod_spec(pod_spec))
    }

    fn namespace(&self) -> Result<String, Error> {
        <Self as ResourceExt>::namespace(self).ok_or(Error::FieldNotFound(KubeFields::Namespace))
    }
}

/// Retrieves all containers within a Pod specification.
fn get_all_containers_in_pod_spec(pod_spec: &PodSpec) -> Vec<&Container> {
    let containers = &pod_spec.containers;
    let init_containers = pod_spec.init_containers.iter().flatten();
    containers.iter().chain(init_containers).collect()
}

#[cfg(test)]
mod tests {
    use k8s_openapi::api::apps::v1::Deployment;
    use kube::{
        api::{DeleteParams, PostParams},
        Api, Client, Config,
    };

    use crate::Workload;

    #[tokio::test]
    async fn find_env_var() {
        let kube_config = Config::infer().await.unwrap();
        let kube_client = Client::try_from(kube_config.clone()).unwrap();

        let deploy_api: Api<Deployment> = Api::namespaced(kube_client.clone(), "scratch");

        // clear up test Deployment if it already exists
        if deploy_api.get("test-deploy").await.is_ok() {
            deploy_api
                .delete("test-deploy", &DeleteParams::default())
                .await
                .unwrap();
        }

        // put a deployment into the namespace scratch
        let deployment: Deployment = serde_yaml::from_str(
            r"
            apiVersion: apps/v1
            kind: Deployment
            metadata:
              name: test-deploy
              namespace: scratch
            spec:
              replicas: 1
              selector:
                matchLabels:
                  app: test-deploy
              template:
                metadata:
                  labels:
                    app: test-deploy
                spec:
                  containers:
                  - name: test-container
                    image: nginx
                    env:
                    - name: SAMPLE_VAR
                      value: example
            ",
        )
        .unwrap();

        deploy_api
            .create(&PostParams::default(), &deployment)
            .await
            .unwrap();

        let workload = deploy_api.get("test-deploy").await.unwrap();
        let sample_var = workload.find_env_var("SAMPLE_VAR").unwrap();
        assert_eq!(sample_var.value, Some("example".to_string()));
    }
}
