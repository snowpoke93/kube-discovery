//! A helper struct for handling errors within this module.
use std::{fmt, string::FromUtf8Error};

/// A helper struct for handling errors within this module.
pub enum Error {
    /// Represents an error when a specific field is not found in the KubeFields enum.
    FieldNotFound(KubeFields),
    /// Represents an error when data is not found.
    DataNotFound,
    /// Represents an error specific to the kube crate.
    KubeError(kube::Error),
    /// Represents an error when a FromUtf8Error occurs.
    FromUtf8Error(FromUtf8Error),
    /// Represents an error when a search operation fails.
    SearchFailed,
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self, f)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Self::FieldNotFound(ref field) => write!(f, "Field not found: {field:?}"),
            Self::DataNotFound => write!(f, "Data not found"),
            Self::KubeError(ref err) => write!(f, "Kubernetes error: {err}"),
            Self::FromUtf8Error(ref err) => write!(f, "UTF-8 conversion error: {err}"),
            Self::SearchFailed => write!(f, "Search failed"),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match *self {
            Self::KubeError(ref err) => Some(err),
            Self::FromUtf8Error(ref err) => Some(err),
            _ => None,
        }
    }
}

impl From<kube::Error> for Error {
    fn from(err: kube::Error) -> Self {
        Self::KubeError(err)
    }
}

impl From<FromUtf8Error> for Error {
    fn from(err: FromUtf8Error) -> Self {
        Self::FromUtf8Error(err)
    }
}

/// Fields within a Kubernetes manifest.
#[derive(Debug, Copy, Clone)]
pub enum KubeFields {
    /// Represents the pod.spec field within a Kubernetes manifest.
    PodSpec,
    /// Represents the deployment.spec field within a Kubernetes manifest.
    DeploymentSpec,
    /// Represents the statefulset.spec field within a Kubernetes manifest.
    StatefulSetSpec,
    /// Represents the Service field within a Kubernetes manifest.
    Service,
    /// Represents the EnvVarSource field within a Kubernetes manifest.
    EnvVarSource,
    /// Represents the SecretKeySelector field within a Kubernetes manifest.
    SecretKeySelector,
    /// Represents the Authority field within a Kubernetes manifest.
    Authority,
    /// Represents the service.spec field within a Kubernetes manifest.
    ServiceSpec,
    /// Represents the ServicePort field within a Kubernetes service manifest.
    ServicePort,
    /// Represents the NodePort field within a Kubernetes service manifest.
    NodePort,
    /// Represents the Namespace field within a Kubernetes manifest.
    Namespace,
    /// Represents the ClusterIP field within a service manifest.
    ClusterIP,
}
