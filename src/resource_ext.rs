//! Additional functions on Kubernetes [`kube::Resource`] Objects.

use k8s_openapi::{
    api::core::v1::{Secret, Service, ServicePort},
    Metadata,
};
use kube::Config;
use secrecy::SecretString;

use crate::{error::KubeFields, Error};

// I don't want to add `async_trait` as a dependency here, so instead I'm using the macro's expanded code.

/// Additional functions for [`Secret`]s.
pub trait SecretExt {
    /// Loads a value from a Kubernetes Secret. Instead of searching by labels, this function uses a Secret's namespace and name. If found, the value is returned wrapped in a [`SecretString`].
    fn value(&self, key: impl AsRef<str>) -> Result<SecretString, Error>;
}
impl SecretExt for Secret {
    fn value(&self, key: impl AsRef<str>) -> Result<SecretString, Error> {
        let secret_data = &self.data.as_ref().ok_or(Error::DataNotFound)?;
        let raw_value = secret_data[key.as_ref()].clone();
        let value = String::from_utf8(raw_value.0)?;
        Ok(SecretString::new(value))
    }
}
/// Additional functions on resources of Type [`Service`].
pub trait ServiceExt {
    /// Try to get a service's node port.
    fn get_nodeport(&self) -> Result<i32, Error>;
    /// Try to get a service's ClusterIP port.
    fn get_service_port(&self) -> Result<i32, Error>;
    /// Try to get a service's fully qualified domain name.
    fn get_fqdn(&self) -> Result<String, Error>;
    /// Try to get a service's ClusterIP.
    fn get_cluster_ip(&self) -> Result<&str, Error>;
}

impl ServiceExt for Service {
    fn get_nodeport(&self) -> Result<i32, Error> {
        self.iter_ports()
            .find_map(|svc_port| svc_port.node_port)
            .ok_or(Error::FieldNotFound(KubeFields::NodePort))
    }

    fn get_service_port(&self) -> Result<i32, Error> {
        Ok(self.iter_ports().next().ok_or(Error::DataNotFound)?.port)
    }

    fn get_fqdn(&self) -> Result<String, Error> {
        let metadata = self.metadata();
        if let (Some(name), Some(namespace)) = (metadata.name.as_ref(), metadata.namespace.as_ref())
        {
            Ok(format!("{name}.{namespace}.svc.cluster.local"))
        } else {
            Err(Error::DataNotFound)
        }
    }

    fn get_cluster_ip(&self) -> Result<&str, Error> {
        Ok(self
            .spec
            .as_ref()
            .ok_or(Error::FieldNotFound(KubeFields::ServiceSpec))?
            .cluster_ip
            .as_ref()
            .ok_or(Error::FieldNotFound(KubeFields::ClusterIP))?
            .as_str())
    }
}
trait ServiceExtPrivate {
    /// Return an iterator over all `ServicePort` items in the service declaration.
    fn iter_ports(&self) -> Box<dyn Iterator<Item = &ServicePort> + '_>;
}

impl ServiceExtPrivate for Service {
    fn iter_ports(&self) -> Box<dyn Iterator<Item = &ServicePort> + '_> {
        let empty_iter = Box::new(std::iter::empty::<&ServicePort>());
        let Some(service_spec) = self.spec.as_ref() else {return empty_iter};
        let Some(ports) = service_spec.ports.as_ref() else {return empty_iter};

        Box::new(ports.iter())
    }
}

/// Additional functionality on items of type [`Config`].
pub trait ConfigExt {
    /// Return the IP address of the Kubernetes cluster.
    fn cluster_ip(&self) -> Result<String, Error>;
    /// Identify if the current program is being run within the cluster that we are connected to.
    fn is_external_cluster(&self) -> Result<bool, Error>;
}

impl ConfigExt for Config {
    fn cluster_ip(&self) -> Result<String, Error> {
        let kube_authority = self
            .cluster_url
            .authority()
            .ok_or(Error::FieldNotFound(KubeFields::Authority))?;
        Ok(kube_authority.host().to_string())
    }

    fn is_external_cluster(&self) -> Result<bool, Error> {
        Ok(std::env::var("KUBERNETES_SERVICE_HOST") != Ok(self.cluster_ip()?))
    }
}
