//! Main functionality of this crate.

use crate::{error::KubeFields, ConfigExt, Error, SecretExt, ServiceExt, Workload};
use k8s_openapi::{
    api::{
        apps::v1::{Deployment, StatefulSet},
        core::v1::{Secret, Service},
    },
    serde::de::DeserializeOwned,
};
use kube::{
    api::{Api, ListParams},
    core::Resource,
    Client, Config,
};
use secrecy::SecretString;
use std::fmt::Debug;

#[cfg(test)] // for doctests
use {serde_yaml as _, tokio as _};

/// Functionality on resources that we select by label.
#[derive(Debug)]
pub struct LabelSelector<L>(pub L)
where
    L: AsRef<str> + Send + Sync;
impl<L> LabelSelector<L>
where
    L: AsRef<str> + Send + Sync,
{
    /// A function that searches for a Kubernetes object by its labels.
    /// You can specify the type of object by providing the an appropriate type for `K`.
    /// The function returns the first object that has the provided labels, if one exists.
    pub async fn find_resource<K>(&self, client: &Client) -> Result<K, Error>
    where
        K: Resource + Clone + DeserializeOwned + Debug,
        K::DynamicType: Default,
    {
        let api: Api<K> = Api::all(client.clone());
        let lp = ListParams::default().labels(self.0.as_ref());
        api.list(&lp)
            .await?
            .into_iter()
            .next()
            .ok_or(Error::SearchFailed)
    }

    /// Find the first workload that matches the provided labels.
    async fn find_workload(&self, kube_client: &Client) -> Result<Box<dyn Workload>, Error> {
        if let Ok(deployment) = self.find_resource::<Deployment>(kube_client).await {
            Ok(Box::new(deployment))
        } else if let Ok(statefulset) = self.find_resource::<StatefulSet>(kube_client).await {
            Ok(Box::new(statefulset))
        } else {
            Err(Error::SearchFailed)
        }
    }

    // TODO: This should be a method on `Workload`
    /// Based on a secret ref provided within a Workload, load the value of the secret.
    // #lizard forgives the complexity
    pub async fn load_secret_value_through_workload(
        &self,
        kube_client: &Client,
        secret_name: impl AsRef<str> + Send,
    ) -> Result<SecretString, Error> {
        let workload: Box<dyn Workload> = self.find_workload(kube_client).await?;

        let password_secret_ref = workload.find_secret_ref(secret_name.as_ref())?;

        let password_secret_namespace = workload.namespace()?;

        let password_secret_name = password_secret_ref
            .name
            .ok_or(Error::FieldNotFound(KubeFields::SecretKeySelector))?;
        let password_secret_key = &password_secret_ref.key;

        let secret_api = Api::<Secret>::namespaced(kube_client.clone(), &password_secret_namespace);

        secret_api
            .get(&password_secret_name)
            .await?
            .value(password_secret_key)
    }

    /// Searches for a service by labels, then returns an address to the service port.
    /// If the environment variable `KUBERNETES_SERVICE_HOST` exists and the Cluster's IP is equal to it, then it will use the regular service port and the ClusterIP.
    /// Otherwise, the node port and the cluster's external IP will be used.
    /// This assumes that the first service that is found with the labels is of type `NodePort`.
    //  #lizard forgives the complexity
    pub async fn load_service_host(
        &self,
        kube_config: &Config,
        kube_client: &Client,
    ) -> Result<String, Error> {
        let service = self.find_resource::<Service>(kube_client).await?;

        // If the current program is being run within the cluster that contains the service, then the service address is "service_cluster_ip:service_port".
        // If the program is reaching an external cluster, then the service is reached under "kube_cluster_ip::node_port"
        let host = if kube_config.is_external_cluster()? {
            let kube_cluster_ip = kube_config.cluster_ip()?; // cluster's external ip
            let node_port = service.get_nodeport()?;
            format!("{kube_cluster_ip}:{node_port}")
        } else {
            let service_cluster_ip = service.get_cluster_ip()?; // ClusterIP field within the service
            let service_port = service.get_service_port()?;
            format!("{service_cluster_ip}:{service_port}")
        };

        Ok(host)
    }
}
