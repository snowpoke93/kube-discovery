//! ## Use case:
//! "I've got a test setup on my Kubernetes cluster, but I don't want to keep updating my environment variables with locations and authentication info. Can't I just access the cluster and load relevant data from there?"
//!
//! ## Limitations
//! This crate is written for my specific use case, so some of the assumptions it makes might not make sense for other setups.
//!
//! ## Example (Location Discovery)
//! ```
//! use kube_discovery::k8s_openapi::api::core::v1::Service;
//! use kube_discovery::kube::{
//!     api::{DeleteParams, PostParams},
//!     Api, Client, Config,
//! };
//! use kube_discovery::*;
//!
//! #[tokio::main]
//! async fn main() {
//!     // doctest setup
//!     let kube_config = Config::infer().await.unwrap();
//!     let kube_client = Client::try_from(kube_config.clone()).unwrap();
//!     let svc_api: Api<Service> = Api::namespaced(kube_client.clone(), "scratch");
//!
//!     // Say you've got a service like this
//!     let service_with_nodeport: Service = serde_yaml::from_str(
//!         "
//!     apiVersion: v1
//!     kind: Service
//!     metadata:
//!       name: doctest-example
//!       labels:
//!         app: doctest-example
//!     spec:
//!       type: NodePort
//!       selector:
//!         app: example-app
//!       ports:
//!       - protocol: TCP
//!         port: 80
//!         targetPort: 8080
//!         nodePort: 31234
//!     ",
//!     )
//!     .unwrap();
//!     svc_api
//!         .create(&PostParams::default(), &service_with_nodeport)
//!         .await
//!         .unwrap();
//!
//!     // Instead of storing the service's public url in some environment variable
//!     // (or, heavens forbid, even hardcoding it), just load the location based
//!     // on the service's labels.
//!     let service_uri = LabelSelector("app=doctest-example")
//!         .load_service_host(&kube_config, &kube_client)
//!         .await
//!         .unwrap();
//!
//!     assert_eq!(service_uri, "162.55.38.94:31234");
//!
//!     // doctest cleanup
//!     svc_api
//!         .delete("doctest-example", &DeleteParams::default())
//!         .await
//!         .unwrap();
//! }
//! ```
#![warn(clippy::all, clippy::pedantic, clippy::nursery)]
#![warn(
    warnings,
    future_incompatible,
    nonstandard_style,
    rust_2018_compatibility,
    rust_2018_idioms,
    unused,
    missing_docs,
    missing_copy_implementations,
    missing_debug_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unused_crate_dependencies,
    unused_extern_crates,
    variant_size_differences
)]
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::wildcard_imports)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::doc_markdown)]

pub mod error;
mod label_selector;
mod resource_ext;
pub mod workload;

pub use crate::label_selector::*;
pub use error::Error;
pub use k8s_openapi;
pub use kube;
pub use resource_ext::ConfigExt;
pub use resource_ext::SecretExt;
pub use resource_ext::ServiceExt;
pub use workload::Workload;

// TODO: This crate desperately needs tests
